package app

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func toJSON(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		sendResponse(w, http.StatusInternalServerError, fmt.Sprintf("Decoding JSON: %v", err))
	}
}

func sendResponse(w http.ResponseWriter, status int, msg string) {
	w.WriteHeader(status)
	w.Write([]byte(msg))
}

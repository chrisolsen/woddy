package app

import (
	"net/http"

	"golang.org/x/net/context"
	"google.golang.org/appengine/datastore"

	"github.com/chrisolsen/ae"
	"github.com/chrisolsen/ae/que"
	"github.com/chrisolsen/ae/que/headers"
)

func init() {

	secHeaders := map[string]string{
		"x-Content-Type-Options":    "nosniff",
		"x-xss-protection":          "1; mode=block",
		"strict-transport-security": "max-age=31536000; includeSubDomains",
		"X-Frame-Options":           "SAMEORIGIN",
		"Referrer-Policy":           "strict-origin",
	}

	mw := que.New(
		headers.SetMulti(secHeaders),
		ae.OriginMiddleware([]string{"http://localhost:3000"}),
	)

	http.HandleFunc("/", mw.HandleFunc(func(c context.Context, w http.ResponseWriter, r *http.Request) {
		route := ae.NewRoute(r)

		switch {

		// routines
		case route.Matches("GET", "/routines"):
			handleGetRoutines(c, w, r)
		case route.Matches("GET", "/routines/:id"):
			handleGetRoutine(c, w, r, route.Key("id"))

		// videos
		case route.Matches("GET", "/videos"):
			handleGetVideos(c, w, r)

		default:
			w.WriteHeader(http.StatusNotFound)
		}
	}))
}

func handleGetVideos(c context.Context, w http.ResponseWriter, r *http.Request) {
	videos := []*Video{
		&Video{
			Model:    ae.Model{Key: datastore.NewIncompleteKey(c, "videos", nil)},
			Exercise: "Burpees",
			URL:      "https://www.youtube.com/embed/Pf7wZvraWV0",
			Tags:     []string{"burpee"},
		},
		&Video{
			Model:    ae.Model{Key: datastore.NewIncompleteKey(c, "videos", nil)},
			Exercise: "Pushups",
			URL:      "https://www.youtube.com/embed/IODxDxX7oi4",
			Tags:     []string{"pushup", "push up", "push-up"},
		},
		&Video{
			Model:    ae.Model{Key: datastore.NewIncompleteKey(c, "videos", nil)},
			Exercise: "Situps",
			URL:      "https://www.youtube.com/embed/jDwoBqPH0jk",
			Tags:     []string{"situp", "sit up", "sit-up"},
		},
		&Video{
			Model:    ae.Model{Key: datastore.NewIncompleteKey(c, "videos", nil)},
			Exercise: "Pullups",
			URL:      "https://www.youtube.com/embed/QZhzZtExnfc",
			Tags:     []string{"pullup", "pull up", "pull-up"},
		},
	}

	toJSON(w, videos)
}

func handleGetRoutines(c context.Context, w http.ResponseWriter, r *http.Request) {
	routines := []*Routine{}
	keys, err := datastore.NewQuery(RoutineTable).GetAll(c, &routines)
	if err != nil {
		sendResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	for i, k := range keys {
		routines[i].Key = k
	}

	// TODO: remove stubbed data
	routines = []*Routine{
		&Routine{
			Model:       ae.Model{Key: datastore.NewIncompleteKey(c, "routines", nil)},
			Name:        "Calisthenics 1",
			Description: "Entry level routine for people who need to gain the strength for more advanced Calisthenic exercises",
			Tags:        []string{"strength", "weight-loss"},
		},
		&Routine{
			Model:       ae.Model{Key: datastore.NewIncompleteKey(c, "routines", nil)},
			Name:        "Calisthenics 2",
			Description: "Next level routine for people who have the required starting level strength, but need to get down some of the basic exercises used in the more advanced classes",
			Tags:        []string{"strength", "weight-loss"},
		},
		&Routine{
			Model:       ae.Model{Key: datastore.NewIncompleteKey(c, "routines", nil)},
			Name:        "Calisthenics 3",
			Description: "Advnaced level routine for people who have both the strength and technique down.",
			Tags:        []string{"strength", "weight-loss"},
		},
		&Routine{
			Model:       ae.Model{Key: datastore.NewIncompleteKey(c, "routines", nil)},
			Name:        "Burn that fat",
			Description: "This routine focuses on the proper tehnique and intensity to perform exercises with that maximize the burning of fat.",
			Tags:        []string{"weight-loss"},
		},
	}

	toJSON(w, routines)
}

func handleGetRoutine(c context.Context, w http.ResponseWriter, r *http.Request, key *datastore.Key) {
	var routine *Routine

	// if key == nil {
	// 	sendResponse(w, http.StatusBadRequest, "routine key is required")
	// 	return
	// }

	// err := datastore.Get(c, key, routine)
	// if err != nil {
	// 	sendResponse(w, http.StatusInternalServerError, err.Error())
	// 	return
	// }

	// TODO: this also needs the workouts
	routine = &Routine{
		Model: ae.Model{Key: datastore.NewIncompleteKey(c, "routines", nil)},
		Name:  "Calisthenics",
		Tags:  []string{"core", "strength"},

		Workouts: []Workout{
			Workout{Details: "5 rounds of the following\n* 50 pushups"},
			Workout{Details: "100 pushups"},
			Workout{Details: "100 pullups"},
			Workout{Details: "100 situps"},
		},
	}
	toJSON(w, routine)
}

package app

import (
	"time"

	"github.com/chrisolsen/ae"
)

const RoutineTable = "routines"

type Routine struct {
	ae.Model

	Name        string    `json:"name"`
	Tags        []string  `json:"tags" datastore:",noindex"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"-"`

	Workouts []Workout `json:"workouts" datastore:"-"`
}

const WorkoutTable = "workouts"

type Workout struct {
	ae.Model

	Details   string    `json:"details"`
	CreatedAt time.Time `json:"-"`
}

const VideoTable = "videos"

type Video struct {
	ae.Model
	Exercise  string    `json:"exercise"`
	URL       string    `json:"url" datastore:",noindex"`
	Tags      []string  `json:"tags"`
	CreatedAt time.Time `json:"-"`
}

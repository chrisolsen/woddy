const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const merge = require('merge');
const webpack = require('webpack');

module.exports = (env) => {

  const isProd = env.production;

  const prodConfig = {
      plugins: [
          new UglifyJsPlugin({
              include: /dist/,
              exclude: [/node_modules/, /elm-stuff/]
          }),
      ]
  }

  const baseConfig = {
    entry: './src/index.js',

    output: {
      path: path.join( __dirname, '../api/src/public' ),
      filename: 'index.js',
    },

    resolve: {
      extensions: ['.js', '.elm']
    },

    module: {
      rules: [
        {
          test: /\.elm$/,
          exclude: [/elm-stuff/, /node_modules/],
          use: [
            'elm-hot-loader',
            'elm-webpack-loader'
          ]
        },
        {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    'css-loader', 
                    'sass-loader'
                ],
                publicPath: '/dist'
            })
        },
      ]
    },

    plugins: [
      new HtmlWebpackPlugin({ 
        hash: true,
        env: isProd ? "prod" : "dev",
        foo : "bar",
        template: './src/index.html',
        // favicon: '',
        // minify: {}, 
      }),
      new CopyWebpackPlugin([
          { from: "src/assets", to: "assets" },
      ]),
      new ExtractTextPlugin({
        filename: 'app.css',
        disable: !isProd,  // embedded styles will allow for immediate browser updates
      }),
    ],

    target: 'web',

    devServer: {
      inline: true,
      stats: {
          colors: true
      },
    }
  };

  return isProd ? merge(prodConfig, baseConfig) : baseConfig;
};

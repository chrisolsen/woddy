module Colors exposing (..)

import Css exposing (..)


colorPrimary : Css.Color
colorPrimary =
    hsl 212 70 50


colorPrimaryDark : Css.Color
colorPrimaryDark =
    hsl 212 70 38


colorPantone1 : Css.Color
colorPantone1 =
    hsl 43 1 3


colorPantone2 : Css.Color
colorPantone2 =
    hsl 84 1 3


colorPantone3 : Css.Color
colorPantone3 =
    hsl 16 1 3


colorPantone4 : Css.Color
colorPantone4 =
    hsl 7 1 3


colorGrey7 : Css.Color
colorGrey7 =
    rgb 55 67 85


colorGrey6 : Css.Color
colorGrey6 =
    rgb 61 77 101


colorGrey5 : Css.Color
colorGrey5 =
    rgb 83 101 125


colorGrey4 : Css.Color
colorGrey4 =
    rgb 131 147 167


colorGrey3 : Css.Color
colorGrey3 =
    rgb 173 185 201


colorGrey2 : Css.Color
colorGrey2 =
    rgb 201 211 223


colorGrey1 : Css.Color
colorGrey1 =
    rgb 235 239 243


colorWhite : Css.Color
colorWhite =
    rgb 255 255 255

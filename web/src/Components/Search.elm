module Components.Search exposing (..)

import Components.Modal as Modal
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as Json
import Models exposing (Routine, Routines)
import RemoteData exposing (..)
import Route exposing (routinePath)


type Msg
    = SearchInputMsg String
    | SearchPerformMsg
    | SearchCloseMsg


type alias Model =
    { input : String
    , filter : Maybe String
    , routines : WebData Routines
    , message : Maybe String
    }


init : Model
init =
    { input = ""
    , filter = Nothing
    , routines = NotAsked
    , message = Nothing
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SearchInputMsg input ->
            let
                -- show the *nothing found* message if the input is blank
                f =
                    if String.length input == 0 then
                        Nothing
                    else
                        model.filter
            in
            ( { model
                | input = input
                , filter = f
              }
            , Cmd.none
            )

        SearchPerformMsg ->
            ( { model
                | filter = Just model.input
              }
            , Cmd.none
            )

        SearchCloseMsg ->
            ( model, Cmd.none )


view : Model -> Html Msg
view model =
    let
        searchBox =
            div [ class "search" ]
                [ input
                    [ type_ "search"
                    , class "search-input"
                    , placeholder "Find workouts"
                    , autofocus True
                    , onInput SearchInputMsg
                    , onEnter SearchPerformMsg
                    ]
                    []
                , filteredRoutines model
                ]
    in
    Modal.view
        { content = Just searchBox
        , closeMessage = SearchCloseMsg
        }


filteredRoutines : Model -> Html Msg
filteredRoutines model =
    case model.routines of
        NotAsked ->
            text ""

        Loading ->
            text "Loading..."

        Success routines ->
            case model.filter of
                Just filter ->
                    let
                        routineFilter r =
                            (String.contains (String.toLower filter) (String.toLower r.description)) 
                                || (String.contains (String.toLower filter) (String.toLower r.name))

                        toElem r =
                            li []
                                [ a
                                    [ href (routinePath r.key)
                                    -- , onClick SearchCloseMsg  -- this causes a flicker due to a double render
                                    ]
                                    [ div
                                        [ class "search-item-title" ]
                                        [ text r.name ]
                                    , div
                                        [ class "search-item-details" ]
                                        [ text r.description ]
                                    ]
                                ]

                        filteredRoutines =
                            List.filter routineFilter routines
                    in

                    if List.length filteredRoutines > 0 then
                        List.map toElem filteredRoutines
                            |> ul [ class "search-results" ]
                    else
                        div [ class "search-message" ] [ text "No workouts match your search" ]


                Nothing ->
                    text "" 

        Failure _ ->
            text "Fail!!"


onEnter : Msg -> Attribute Msg
onEnter msg =
    let
        isEnter code =
            if code == 13 then
                Json.succeed msg
            else
                Json.fail "not ENTER"
    in
    on "keydown" (Json.andThen isEnter keyCode)

module Components.Modal exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)

type alias Model msg =
    { content : Maybe (Html msg)
    , closeMessage : msg
    }

view : Model msg -> Html msg
view model =
    div [ class "modal" ]
        [ div 
            [ class "modal-background" ]
            []
        , div 
            [ class "modal-content" ]
            [ renderContent model ]
        , button 
            [ class "modal-close", onClick model.closeMessage ]
            [ img [ src "assets/ic_close.svg" ] []
            ]
        ]

renderContent : Model msg -> Html msg
renderContent model =
    Maybe.withDefault (text "") model.content

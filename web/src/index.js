'use strict'

require('./Styles/app.scss');
var config = require('../config.json');

var Elm = require('./Main.elm');
var now = new Date();
var tzOffset = now.getTimezoneOffset() * 60 * 1000 * -1;
var msPerDay = 24 * 60 * 60 * 1000;
var dayIndex = Math.floor((now.getTime() + tzOffset ) / msPerDay);

Elm.Main.embed(document.getElementById('main'), {
    dayIndex: dayIndex,
    apiHost: config[env].apiHost,
});

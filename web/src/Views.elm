module Views exposing (view)

-- import Html exposing (..)
-- import Html.Attributes exposing (..)
-- import Models exposing (Message)
-- import Msgs exposing (Msg(..))
-- import Page.Landing as Landing
-- import Page.Routine as Routine
-- import Page.Routines as Routines
-- import Route exposing (Route(..))


-- view : App -> Html Msg
-- view model =
--     div []
--         [ headerView
--         , messageView model.message
--         , content model
--         ]


-- headerView : Html Msg
-- headerView =
--     header [ id "app-header" ]
--         [ div [ id "app-header-name" ]
--             [ text "Woddy" ]
--         , a [ id "app-header-actions" ]
--             [ img [ src "assets/ic_search.svg" ] []
--             ]
--         ]


-- content : App -> Html Msg
-- content model =
--     case model.route of
--         LandingRoute ->
--             Landing.view model.landingPage

--         TagRoute tag ->
--             Routines.view model.routines tag

--         RoutineRoute _ ->
--             renderRoutine model

--         NotFoundRoute ->
--             text "Page not found"


-- renderRoutine : App -> Html Msg
-- renderRoutine model =
--     Routine.view
--         { dayIndex = model.dayIndex
--         , videos = model.videos
--         , routine = model.routine
--         }


-- messageView : Maybe Message -> Html Msg
-- messageView message =
--     case message of
--         Just msg ->
--             div []
--                 [ text msg.content ]

--         Nothing ->
--             text ""

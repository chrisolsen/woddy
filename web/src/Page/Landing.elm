module Page.Landing exposing (..)

import Html exposing (..)
import Html.Attributes exposing (class, href, id)
import Route exposing (Route(..), tagPath)


type alias Tag =
    { name : String
    , slug : String
    }

type alias Model =
    List Tag


init : Model
init =
    [ { name = "Strength", slug = "strength" }
    , { name = "Cardio", slug = "cardio" }
    , { name = "Weight Loss", slug = "weight-loss" }
    ]


view : Model -> Html msg
view tags =
    div [ class "page" ]
        [ h1 []
            [ text "Let's Get Stronger!" ]
        , p []
            [ text "orem ipsum dolor sit amet, consectetur adipiscing elit. Aenean varius vitae nibh quis vehicula. Etiam tempor vulputate odio sit amet dapibus. Donec tincidunt eros quis urna mattis eleifend. Vivamus et luctus lacus. Praesent felis augue, fringilla eget eros a, dignissim lobortis arcu. Nam bibendum risus nec sapien consequat, vitae aliquet nibh finibus." ]
        , tagList tags
        ]


tagList : List Tag -> Html msg
tagList tags =
    let
        link =
            \tag ->
                a
                    [ class "category"
                    , href <| tagPath tag.slug
                    ]
                    [ text tag.name ]
    in
    section [ class "categories" ]
        (List.map link tags)

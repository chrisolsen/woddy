module Page.Routines exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Http exposing (decodeUri)
import Models exposing (Routines, RoutinesPayload)
import RemoteData exposing (..)
import Route exposing (routinePath)
import String


type alias Model =
    { routines : RoutinesPayload
    }


type
    Msg
    -- = FetchSuccess (Result Http.Error Routines)
    = Noop


update : Model -> Msg -> ( Model, Cmd Msg )
update model msg =
    case msg of
        Noop ->
            ( model, Cmd.none )


init : Model
init =
    { routines = NotAsked
    }


filterByTag : Routines -> String -> Maybe Routines
filterByTag routines tag =
    let
        filtered =
            List.filter
                (\p ->
                    List.member tag p.tags
                )
                routines
    in
    if List.length filtered > 0 then
        Just filtered
    else
        Nothing


view : Model -> String -> Html msg
view model tag =
    let
        title =
            case decodeUri tag of
                Just str ->
                    str |> String.toUpper

                Nothing ->
                    ""
    in
    div []
        [ h1 [] [ text (title ++ " PROGRAMS") ]
        , routinesListView model.routines tag
        ]


routinesListView : RoutinesPayload -> String -> Html msg
routinesListView model tag =
    case model of
        NotAsked ->
            text ""

        Loading ->
            div []
                [ text "Loading..." ]

        Success routines ->
            let
                decodedTag =
                    Maybe.withDefault "" (decodeUri tag)

                filteredRoutines =
                    filterByTag routines decodedTag
            in
            case filteredRoutines of
                Just routines ->
                    routines
                        |> List.map
                            (\p ->
                                a [ href <| routinePath p.key ]
                                    [ h2 
                                        [ class "routine-name" ]
                                        [ text p.name ]
                                    , Html.p
                                        [ class "routine-description" ]
                                        [ text p.description ]
                                    ]
                            )
                        |> div []

                Nothing ->
                    text "no matching routines found"

        Failure _ ->
            div []
                [ text "Failed to fetch routines" ]

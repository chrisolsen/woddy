module Page.Routine exposing (..)

import Array
import Components.Modal as Modal
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Markdown
import Models exposing (Videos, VideosPayload, RoutinePayload)
import RemoteData exposing (..)
import Json.Encode as Encode


type alias Model =
    { dayIndex : Int
    , videos : VideosPayload
    , routine : RoutinePayload
    , videoUrl : Maybe String
    }


type Msg
    = CloseModal
    | ShowVideo String
    | Noop


reset : Model -> Model
reset model =
    { model
        | routine = NotAsked
        , videoUrl = Nothing
        , dayIndex = 0
    }


init : Int -> Model
init dayIndex =
    { dayIndex = dayIndex
    , videos = NotAsked
    , routine = NotAsked
    , videoUrl = Nothing
    }


update : Model -> Msg -> ( Model, Cmd Msg )
update model msg =
    case msg of
        CloseModal ->
            ( { model
                | videoUrl = Nothing
              }
            , Cmd.none
            )

        ShowVideo url ->
            ( { model
                | videoUrl = Just url
              }
            , Cmd.none
            )

        Noop ->
            ( model, Cmd.none )


view : Model -> Html Msg
view model =
    case model.routine of
        NotAsked ->
            text ""

        Loading ->
            text "Loading..."

        Failure _ ->
            div [] [ text "Invalid routine" ]

        Success routine ->
            let
                workouts =
                    Array.fromList routine.workouts

                workoutIndex =
                    model.dayIndex % List.length routine.workouts

                workout =
                    Array.get workoutIndex workouts

                workoutDetails =
                    case workout of
                        Just workout ->
                            workout.details

                        Nothing ->
                            "Not found"

                matchedVideos =
                    case model.videos of
                        RemoteData.Success videos ->
                            videos
                                |> List.filter 
                                    (\video -> 
                                        video.tags 
                                            |> List.any (\tag -> String.contains tag (String.toLower workoutDetails))
                                    )

                        _ ->
                            []
            in
            div []
                [ h1 [] [ text routine.name ]
                , div []
                    [ Markdown.toHtml
                        [ class "workout-details" ]
                        workoutDetails
                    , tagsView matchedVideos
                    , videoView model
                    ]
                ]


videoView : Model -> Html Msg
videoView model =
    case model.videoUrl of
        Just url ->
            let 
                frame = 
                    div [ class "video-container"]
                        [ iframe 
                            [ src url
                            , class "video"
                            , attribute "frameborder" "0"
                            ] []
                        ]
            in
            Modal.view
                { content = Just frame
                , closeMessage = CloseModal
                }

        Nothing ->
            text ""


tagsView : Videos -> Html Msg
tagsView videos =
    videos
        |> List.map
            (\video ->
                li []
                    [ a
                        [ class "pill"
                        , onClick (ShowVideo video.url)
                        ]
                        [ text video.exercise ]
                    ]
            )
        |> ul [ class "tags" ]

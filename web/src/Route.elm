module Route exposing (..)

import Navigation exposing (..)
import UrlParser as Url exposing ((</>), Parser, oneOf, parseHash, s, string, top)


type Route
    = LandingRoute
    | TagRoute String
    | RoutineRoute String
    | NotFoundRoute



-- PRIVATE


route : Parser (Route -> a) a
route =
    oneOf
        [ Url.map LandingRoute top
        , Url.map TagRoute (s "tags" </> string)
        , Url.map RoutineRoute (s "routines" </> string)
        ]



-- PUBLIC


landingPath : String
landingPath =
    "#/"


tagPath : String -> String
tagPath tag =
    "#/tags/" ++ tag


routinePath : String -> String
routinePath id =
    "#/routines/" ++ id


parseLocation : Navigation.Location -> Route
parseLocation location =
    case parseHash route location of
        Just route ->
            route

        Nothing ->
            NotFoundRoute

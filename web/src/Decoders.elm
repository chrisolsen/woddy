module Decoders exposing (..)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Pipeline exposing (decode, required, optional)
import Models 

videoDecoder : Decoder Models.Video
videoDecoder =
    decode Models.Video
        |> required "key" Decode.string
        |> required "exercise" Decode.string
        |> required "tags" (Decode.list Decode.string)
        |> required "url" Decode.string


videosDecoder : Decoder Models.Videos 
videosDecoder =
    Decode.list videoDecoder 


routinesDecoder : Decoder Models.Routines 
routinesDecoder =
    Decode.list routineDecoder 


routineDecoder : Decoder Models.Routine
routineDecoder =
    decode Models.Routine
        |> required "key" Decode.string
        |> required "name" Decode.string
        |> required "tags" (Decode.list Decode.string)
        |> required "description" Decode.string
        |> optional "workouts" (Decode.list workoutDecoder) []


workoutDecoder : Decoder Models.Workout
workoutDecoder =
    decode Models.Workout
        |> required "details" Decode.string

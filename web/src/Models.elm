module Models exposing (..)

import RemoteData exposing (WebData)


type alias Routine =
    { key : String
    , name : String
    , tags : List String
    , description : String
    , workouts : List Workout
    }


type alias Video =
    { key : String
    , exercise : String
    , tags : List String
    , url : String
    }


type alias Workout =
    { details : String }


type alias Videos =
    List Video


type alias Routines =
    List Routine


type alias RoutinesPayload =
    WebData Routines


type alias RoutinePayload =
    WebData Routine


type alias VideosPayload =
    WebData Videos


type alias Message =
    { content : String
    , type_ : String
    }

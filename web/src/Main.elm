module Main exposing (..)

import Debug exposing (log)
import Components.Search
import Decoders exposing (videosDecoder, routineDecoder, routinesDecoder)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (keyCode, on, onClick, onInput)
import Http
import HttpBuilder exposing (..)
import Models exposing (..)
import Navigation exposing (..)
import Page.Landing
import Page.Routine
import Page.Routines
import Process
import RemoteData exposing (..)
import Route exposing (Route(..), parseLocation)
import Task
import Time


type Msg
    = OnLocationChange Location
    | ClearMessage
    | ShowSearchMsg
    | FetchVideosCompleted (Result Http.Error Videos)
    | FetchRoutinesCompleted (Result Http.Error Routines)
    | FetchRoutineCompleted (Result Http.Error Routine)
    | RoutinesMsg Page.Routines.Msg
    | RoutineMsg Page.Routine.Msg
    | SearchMsg Components.Search.Msg
    | SearchRoutinesMsg (Result Http.Error Routines)


type alias App =
    { route : Route
    , message : Maybe Message
    , landingPage : Page.Landing.Model
    , routinesPage : Page.Routines.Model
    , routinePage : Page.Routine.Model
    , showSearch : Bool
    , search : Components.Search.Model
    , config : Config
    }


type alias Config =
    { dayIndex : Int
    , apiHost : String
    }


clearMessage : Cmd Msg
clearMessage =
    Process.sleep (5 * Time.second)
        |> Task.perform (\_ -> ClearMessage)


update : Msg -> App -> ( App, Cmd Msg )
update msg model =
    case msg of
        OnLocationChange location ->
            let
                newRoute =
                    parseLocation location

                cmd =
                    case newRoute of
                        LandingRoute ->
                            Cmd.none

                        TagRoute tag ->
                            fetchRoutines model.config.apiHost tag 

                        RoutineRoute id ->
                            fetchRoutine model.config.apiHost id

                        NotFoundRoute ->
                            Cmd.none

            in
            ( { model
                | route = newRoute
                , routinePage = Page.Routine.reset model.routinePage 
                , search = Components.Search.init
                , showSearch = False
              }
            , cmd
            )

        ClearMessage ->
            ( { model
                | message = Nothing
              }
            , Cmd.none
            )

        ShowSearchMsg ->
            ( { model
                | showSearch = True
              }
            , searchRoutines model.config.apiHost "" 
            )

        SearchRoutinesMsg (Ok routines) ->
            let
                search =
                    model.search
            in
            ( { model
                | search = { search | routines = Success routines }
              }
            , Cmd.none
            )

        SearchRoutinesMsg (Err _) ->
            let
                search =
                    model.search
            in
            ( { model
                | search = { search | message = Just "Failed to connect to server" }
              }
            , Cmd.none
            )

        SearchMsg Components.Search.SearchCloseMsg ->
            let 
                search = 
                    model.search 
            in
            ( { model
                | showSearch = False
                , search = 
                    { search 
                        | filter = Nothing
                        , message = Nothing
                    }
              }
            , Cmd.none 
            )

        SearchMsg msg ->
            let
                ( searchModel, cmd ) =
                    Components.Search.update msg model.search
            in
            ( { model
                | search = searchModel
              }
            , Cmd.map SearchMsg cmd
            )

        FetchRoutinesCompleted (Ok routines) ->
            ( { model
                | routinesPage = { routines = Success routines }
              }
            , Cmd.none
            )

        FetchRoutinesCompleted (Err _) ->
            ( { model
                | message = Just { content = "Failed fetching routines", type_ = "error" }
              }
            , clearMessage
            )

        FetchRoutineCompleted (Ok routine) ->
            let
                page =
                    model.routinePage

                updated =
                    { page
                        | routine = Success routine
                    }
            in
            ( { model
                | routinePage = updated
              }
            , Cmd.none
            )

        FetchRoutineCompleted (Err _) ->
            ( { model
                | message = Just { content = "Failed fetching routine", type_ = "error" }
              }
            , clearMessage
            )

        FetchVideosCompleted (Ok videos) ->
            let
                rp =
                    model.routinePage

                page =
                    { rp
                        | videos = Success videos
                    }
            in
            ( { model
                | routinePage = page
              }
            , Cmd.none
            )

        FetchVideosCompleted (Err err) ->
            let
                _ = log "fetch videos err" err
            in
                
            ( { model
                | message = Just { content = "Failed fetching videos", type_ = "error" }
              }
            , clearMessage
            )

        RoutinesMsg msg ->
            let
                ( routinesModel, cmd ) =
                    Page.Routines.update model.routinesPage msg
            in
            ( { model
                | routinesPage = routinesModel
              }
            , Cmd.map RoutinesMsg cmd
            )

        RoutineMsg msg ->
            let
                ( routineModel, cmd ) =
                    Page.Routine.update model.routinePage msg
            in
            ( { model
                | routinePage = routineModel
              }
            , Cmd.map RoutineMsg cmd
            )


main : Program Config App Msg
main =
    Navigation.programWithFlags OnLocationChange
        { init = init
        , view = view
        , update = update
        , subscriptions = always Sub.none
        }


init : Config -> Location -> ( App, Cmd Msg )
init config location =
    let
        currentRoute =
            parseLocation location

        cmds =
            Cmd.batch
                [ fetchVideos config.apiHost
                , Navigation.modifyUrl location.hash
                ]
    in
    ( { route = currentRoute
      , message = Nothing
      , landingPage = Page.Landing.init
      , routinesPage = Page.Routines.init
      , routinePage = Page.Routine.init config.dayIndex
      , showSearch = False
      , search = Components.Search.init
      , config = config 
      }
    , cmds
    )


fetchRoutines : String -> String -> Cmd Msg
fetchRoutines host tag =
    HttpBuilder.get (host ++ "/routines")
        |> withExpect (Http.expectJson routinesDecoder)
        |> send FetchRoutinesCompleted 

searchRoutines : String -> String -> Cmd Msg
searchRoutines host tag =
    HttpBuilder.get (host ++ "/routines")
        |> withExpect (Http.expectJson routinesDecoder)
        |> send SearchRoutinesMsg 


fetchRoutine : String -> String -> Cmd Msg
fetchRoutine host id =
    HttpBuilder.get (host ++ "/routines/" ++ id)
        |> withExpect (Http.expectJson routineDecoder)
        |> send FetchRoutineCompleted


fetchVideos : String -> Cmd Msg
fetchVideos host =
    HttpBuilder.get (host ++ "/videos")
        |> withExpect (Http.expectJson videosDecoder)
        |> send FetchVideosCompleted



-- VIEWS


view : App -> Html Msg
view model =
    let
        defaultViews =
            [ headerView
            , messageView model.message
            , content model
            ]

        views =
            if model.showSearch then
                let
                    searchView = Html.map SearchMsg 
                        (Components.Search.view model.search)
                in
                 searchView :: defaultViews
            else
                defaultViews
    in
    div [] views


headerView : Html Msg
headerView =
    header [ id "app-header" ]
        [ div [ id "app-header-name" ]
            [ a
                [ href "/#" ]
                [ text "Woddy" ]
            ]
        , img
            [ src "assets/ic_search.svg"
            , id "app-header-actions"
            , onClick ShowSearchMsg
            ]
            []
        ]


content : App -> Html Msg
content model =
    case model.route of
        LandingRoute ->
            Page.Landing.view model.landingPage

        TagRoute tag ->
            Page.Routines.view model.routinesPage tag

        RoutineRoute _ ->
            Html.map RoutineMsg
                (Page.Routine.view model.routinePage)

        NotFoundRoute ->
            text "Page not found"


messageView : Maybe Message -> Html Msg
messageView message =
    case message of
        Just msg ->
            div []
                [ text msg.content ]

        Nothing ->
            text ""

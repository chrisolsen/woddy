# WODDY

## Database

```
+---------+
| Program |
+---------+
    |
    |
    \__ +---------+
        | Workout |
        +---------+


+-------+
| Video |
+-------+
```

## Endpoints

### Public
GET programs
    [{id: "1", name: "foo", photoUrl: "...", workoutCount: 99, tags: ["core", ...]}, ...]

GET programs/:id 
    {id: "1", name: "foo", photoUrl: "...", workouts: [{details: "..."}]}

### Admin
GET programs/:pid/workouts/:wid
POST programs/:pid/workouts
PUT programs/:pid/workouts/:wid
DELETE programs/:pid/workouts/:wid

POST programs
PUT programs/:id
DELETE programs/:id

## Web Pages

/
/programs/:id